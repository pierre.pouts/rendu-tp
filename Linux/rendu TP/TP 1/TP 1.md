🌞 **Trouver au moins 4 façons différentes de péter la machine**

Méthode 1: 
```
cd boot/grub2 -> rm grub.cfg 
```
Supprime Grub donc à l'allumage de la vm, le noyau linux ne peux plus se charger avec grub.

Méthode 2 : 
```
nano etc/shadow
```
Modification uniquement du mdp de root.

Méthode 3 : 
```
sudo rm -Rf /* 
```
Supression pure des fichier de partition principale.

Méthode 4:
```
cd root/ -> nano .bashrc/ : :(){ :|:& };:
```
Quand se script est lancé avec un cron au démarrrage, la machine lance des forks à l'infini et donc, elle devient inutilisable.

Méthode 5 :
```
cd root/ -> nano .bashrc/ : sudo killall -u root 
```
empeche de se connecter a root. 


