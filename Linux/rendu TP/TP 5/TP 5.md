# Partie 1 : Mise en place et maîtrise du serveur Web

## 1. Installation

🌞 **Installer le serveur Apache**

```powershell
[skyze@web ~]$ sudo dnf install httpd
```

🌞 **Démarrer le service Apache**
```powershell
[skyze@web ~]$ sudo systemctl start httpd
[skyze@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-03 15:41:06 CET; 23s ago
```  
```powershell
[skyze@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
```powershell
[skyze@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```
```powershell
[skyze@web ~]$ sudo ss -alpnt | grep httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=11183,fd=4),("httpd",pid=11182,fd=4),("httpd",pid=11181,fd=4),("httpd",pid=11179,fd=4))
```
🌞 **TEST**
```powershell
[skyze@web ~]$ sudo journalctl -xe -u httpd
Jan 03 15:41:06 web systemd[1]: Started The Apache HTTP Server.
```
```powershell
[skyze@web ~]$ sudo systemctl status httpd | grep enable
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```
```powershell
[skyze@web ~]$ curl --silent localhost | head -n 5
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
```
```powershell
pierr@LAPTOP-MDL3A6CL MINGW64 ~
$ curl  10.105.1.11

doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```
## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**
```powershell
[skyze@web ~]$ cat /usr/lib/systemd/system/httpd.service
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```
🌞 **Déterminer sous quel utilisateur tourne le processus Apache**
```powershell
[skyze@web conf]$ cat httpd.conf | grep -a  apache
User apache
Group apache
```
```powershell
[skyze@web conf]$ ps -ef | grep apache
apache       755     708  0 19:58 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       758     708  0 19:58 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       759     708  0 19:58 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       760     708  0 19:58 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
skyze       1479    1410  0 20:10 pts/0    00:00:00 grep --color=auto apache
```
```powershell
[skyze@web share]$ ls -al | grep testpage
drwxr-xr-x.   2 root root    24 Jan  3 15:27 testpag
```

🌞 **Changer l'utilisateur utilisé par Apache**
```powershell
[skyze@web etc]$ sudo adduser user1 -d /usr/share/httpd -s /sbin/nologin
adduser: warning: the home directory /usr/share/httpd already exists.
adduser: Not copying any file from skel directory into it.
```
```powershell
[skyze@web etc]$ cat /etc/passwd | grep user1
user1:x:1002:1002::/usr/share/httpd:/sbin/nologin
```
```powershell
[skyze@web conf]$ cat httpd.conf | grep user1
User user1
```
```powershell
[skyze@web ~]$ ps -ef | grep httpd
root         698       1  0 20:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
user1        709     698  0 20:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
user1        710     698  0 20:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
user1        711     698  0 20:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
user1        712     698  0 20:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
skyze       1424    1406  0 20:45 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**
```powershell
[skyze@web conf]$ cat httpd.conf | grep -i listen
Listen 12500
```
```powershell
[skyze@web ~]$ sudo ss -alntp | grep httpd
LISTEN 0      511                *:12500            *:*    users:(("httpd",pid=715,fd=4),("httpd",pid=714,fd=4),("httpd",pid=713,fd=4),("httpd",pid=689,fd=4))
```powershell
[skyze@web conf]$ curl localhost:12500 | head -5
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
<!doctype html>    0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
```
```powershell
pierr@LAPTOP-MDL3A6CL MINGW64 ~
$ curl 10.105.1.11:12500
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
```

# Partie 2 : Mise en place et maîtrise du serveur de base de données


🌞 **Install de MariaDB sur `db.tp5.linux`**
```powershell
[skyze@db ~]$ sudo dnf install mariadb-server
```
```powershell
[skyze@db ~]$ sudo mysql_secure_installation
```
```powershell
[skyze@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
```powershell
[skyze@db ~]$ sudo systemctl start mariadb
```
🌞 **Port utilisé par MariaDB**

```powershell
[skyze@db ~]$ sudo ss -altnp | grep mariadb
[sudo] password for skyze:
LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=804,fd=19))
``` 
```powershell
[skyze@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
```
🌞 **Processus liés à MariaDB**
```powershell
[skyze@db ~]$ ps -ef | grep mariadb
mysql        804       1  0 11:02 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
skyze       1361    1290  0 11:47 pts/0    00:00:00 grep --color=auto mariadb
```
# Partie 3 : Configuration et mise en place de NextCloud


## 1. Base de données


🌞 **Préparation de la base pour NextCloud**
```powershell
MariaDB [(none)]> CREATE USER 'nexcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.018 sec)
```
```powershell
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)
```
```powershell
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.003 sec)
```
```powershell
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 **Exploration de la base de données**
```powershell
[skyze@web ~]$ mysql -u nextcloud -h 10.105.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 23
```
```powershell
[skyze@web ~]$ sudo dnf install mysql-8.0.30-3.el9_0.x86_64
```
```powershell
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)
```


🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**
```powershell
MariaDB [(none)]> SELECT User FROM mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
4 rows in set (0.001 sec)
```


## 2. Serveur Web et NextCloud



🌞 **Install de PHP**
```powershell
[skyze@web conf]$ sudo dnf config-manager --set-enabled crb
```
```powershell
[skyze@web conf]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
```
```powershell
[skyze@web conf]$ dnf module list php
```
```powershell
[skyze@web conf]$ sudo dnf module enable php:remi-8.1 -y
```
```powershell
[skyze@web conf]$ sudo dnf install -y php81-php
```

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```powershell
[skyze@web conf]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```

🌞 **Récupérer NextCloud**

```powershell
[skyze@web www]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip -O
```
```powershell
[skyze@web www]$ ls /var/www/tp5_nextcloud/
3rdparty  config       core      index.html  occ           ocs-provider  resources   themes
apps      console.php  cron.php  index.php   ocm-provider  public.php    robots.txt  updater
AUTHORS   COPYING      dist      lib         ocs           remote.php    status.php  version.php
```
```powershell
[skyze@web tp5_nextcloud]$ sudo chown apache:apache /var/www/tp5_nextcloud/ -R
```

🌞 **Adapter la configuration d'Apache**
```powershell
[skyze@web conf]$ sudo cat httpd.conf | tail -n 18
IncludeOptional conf.d/*.conf

<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Redémarrer le service Apache** 
```powershell
[skyze@web conf]$ sudo systemctl restart httpd
```

## 3. Finaliser l'installation de NextCloud


🌞 **Exploration de la base de données**
```powershell
mysql> SELECT Count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE';
+----------+
| Count(*) |
+----------+
|       95 |
+----------+
1 row in set (0.00 sec)
```






