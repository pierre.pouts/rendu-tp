🌞 **S'assurer que le service `sshd` est démarré**
```powershell
[skyze920@skyze ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: ena>
     Active: active (running) since Mon 2022-12-05 11:49:40 CET; 3min 59s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 17979 (sshd)
      Tasks: 1 (limit: 11119)
     Memory: 2.0M
        CPU: 19ms
     CGroup: /system.slice/sshd.service
             └─17979 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"
```


🌞 **Analyser les processus liés au service SSH**
```powershell
[skyze920@skyze ~]$ ps -ef|grep sshd
root        1230       1  0 11:42 ?        00:00:00 sshd: skyze920 [priv]
skyze920    1234    1230  0 11:42 ?        00:00:05 sshd: skyze920@pts/0
root       17979       1  0 11:49 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
skyze920   37365    1235  0 12:00 pts/0    00:00:00 grep --color=auto sshd
```

🌞 **Déterminer le port sur lequel écoute le service SSH**
```powershell
[skyze920@skyze ~]$ ss | grep ssh
tcp   ESTAB  0      52                       10.4.1.12:ssh          10.4.1.11:58367
```

🌞 **Consulter les logs du service SSH**
```powershell
[skyze920@skyze log]$ sudo cat secure | grep sshd | tail -n 10
Oct 14 11:24:41 localhost sshd[26218]: Server listening on :: port 22.
Dec  5 11:21:05 localhost sshd[706]: Server listening on 0.0.0.0 port 22.
Dec  5 11:21:05 localhost sshd[706]: Server listening on :: port 22.
Dec  5 11:37:30 skyze sshd[708]: Server listening on 0.0.0.0 port 22.
Dec  5 11:37:30 skyze sshd[708]: Server listening on :: port 22.
Dec  5 11:42:26 skyze sshd[1230]: Accepted password for skyze920 from 10.4.1.11 port 58367 ssh2
Dec  5 11:42:26 skyze sshd[1230]: pam_unix(sshd:session): session opened for user skyze920(uid=1000) by (uid=0)
Dec  5 11:49:40 skyze sshd[708]: Received signal 15; terminating.
Dec  5 11:49:40 skyze sshd[17979]: Server listening on 0.0.0.0 port 22.
Dec  5 11:49:40 skyze sshd[17979]: Server listening on :: port 22.
```
## 2. Modification du service

🌞 **Identifier le fichier de configuration du serveur SSH**
```
sshd_config
```

🌞 **Modifier le fichier de conf**
```powershell
[skyze920@skyze ssh]$ sudo cat sshd_config | grep Port
#Port 22
```
```powershell
[skyze920@skyze ssh]`sudo firewall-cmd --reload
```
```powershell
[skyze920@skyze ssh]$ sudo firewall-cmd --list-all | grep 22
  ports: 4185/tcp 22/tcp
```

🌞 **Redémarrer le service**
```powershell
[skyze920@skyze ~]$ sudo systemctl restart firewalld
```

🌞 **Effectuer une connexion SSH sur le nouveau port**
```powershell
PS C:\Users\pierr> ssh skyze920@10.4.1.12 -p 4185
```

# II. Service HTTP

🌞 **Installer le serveur NGINX**

```powershell
[skyze920@skyze ~]$ sudo dnf install nginx
```
```powershell
[skyze920@skyze ssh]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```

🌞 **Démarrer le service NGINX**
```powershell
[skyze920@skyze ~]$ sudo systemctl start nginx
```
🌞 **Déterminer sur quel port tourne NGINX**
```powershell
[skyze920@skyze ~]$ cat /etc/nginx/nginx.conf | grep listen
        listen       80;
        listen       [::]:80;
```
```powershell
[skyze920@skyze ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

🌞 **Déterminer les processus liés à l'exécution de NGINX**
```powershell
[skyze920@skyze ~]$ ps -ef | grep nginx
root        4408       1  0 10:56 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       4409    4408  0 10:56 ?        00:00:00 nginx: worker process
skyze920    4423    4055  0 11:00 pts/0    00:00:00 grep --color=auto nginx
```

🌞 **Euh wait**
```powershell
[skyze920@skyze ~]$ curl http://10.4.1.12:80 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   930k      0 --:--:-- --:--:-- --:--:--  930k
curl: (23) Failed writing body
```

## 2. Analyser la conf de NGINX

🌞 **Déterminer le path du fichier de configuration de NGINX**
```powershell
[skyze920@skyze ~]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 31 16:37 /etc/nginx/nginx.conf
```

🌞 **Trouver dans le fichier de conf**
```powershell
[skyze920@skyze nginx]$ cat nginx.conf | grep "server {" -A 16
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```
```powershell
[skyze920@skyze nginx]$ cat nginx.conf | grep include
include /usr/share/nginx/modules/*.conf;
    include             /etc/nginx/mime.types;
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*.conf;
```
## 3. Déployer un nouveau site web

🌞 **Créer un site web**
```powershell
[skyze920@skyze tp2_linux]$ pwd index.html
/var/www/tp2_linux
```
```powershell
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>MEOW</title>
  <link rel="stylesheet" href="style.css">
  <script src="script.js"></script>
</head>
<body>
  <h1>MEOW mon premier serveur web</h1>
</body>
</html>
```


🌞 **Adapter la conf NGINX**
```powershell
[skyze920@skyze conf.d]$ cat page.conf
server {
  listen 6716;

  root /var/www/tp2_linux;
}
```
```powershell
[skyze920@skyze nginx]$ sudo rm nginx.conf.default
```
```powershell
[skyze920@skyze nginx]$ sudo systemctl restart nginx
```
```powershell
[skyze920@skyze nginx]$ sudo firewall-cmd --add-port=6716/tcp --permanent
```

🌞 **Visitez votre super site web**
```powershell
[skyze920@skyze ~]$ curl http://10.4.1.12:6716
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   158  100   158    0     0  11550      0 --:--:-- --:--:-- --:--:-- 12153<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>MEOW</title>
</head>
<body>
  <h1>MEOW mon premier serveur web</h1>
</body>
</html>
```

# III. Your own services

## 1. Au cas où vous auriez oublié

## 2. Analyse des services existants

🌞 **Afficher le fichier de service SSH**
```powershell
[skyze920@skyze system]$ cat sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS
```

🌞 **Afficher le fichier de service NGINX**
```powershell
[skyze920@skyze system]$ cat nginx.service |grep ExecStart=
ExecStart=/usr/sbin/nginx
``

## 3. Création de service

🌞 **Créez le fichier `/etc/systemd/system/tp2_nc.service`**
```powershell
[skyze920@skyze system]$ cat tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 666
```

🌞 **Indiquer au système qu'on a modifié les fichiers de service**
```powershell
[skyze920@skyze system]$ sudo systemctl daemon-reload
```

🌞 **Démarrer notre service de ouf**
```powershell
[skyze920@skyze system]$ sudo systemctl start tp2_nc
```

🌞 **Vérifier que ça fonctionne**
```powershell
[skyze920@skyze system]$ sudo systemctl status tp2_nc
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Fri 2022-11-25 17:38:32 CET; 1s ago
```
```powershell
[skyze920@skyze ~]$ ss -alnpt | grep 666
LISTEN 0      10           0.0.0.0:666       0.0.0.0:*
LISTEN 0      10              [::]:666          [::]:*
```
🌞 **Les logs de votre service**
```powershell
[skyze920@skyze ~]$ sudo journalctl -xe -u tp2_nc | grep start
 A start job for unit tp2_nc.service has finished successfully.
```
```powershell
[skyze920@skyze ~]$ sudo journalctl -xe -u tp2_nc | grep "SALUT CV"
Nov 26 07:24:53 TP2 nc[2518]: SALUT CV
```
```powershell
[skyze920@skyze ~]$ sudo journalctl -xe -u tp2_nc | grep exit
Nov 26 07:25:41 TP2 systemd[1]: tp2_nc.service: Failed with result 'exit-code'.
```


🌞 **Affiner la définition du service**
```powershell
[skyze920@skyze system]$ sudo systemctl daemon-reload
```

```powershell
Nov 26 09:26:58 TP2 systemd[1]: tp2_nc.service: Failed with result 'exit-code'.
░░ Subject: Unit failed
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ The unit tp2_nc.service has entered the 'failed' state with result 'exit-code'.
Nov 26 09:26:58 TP2 systemd[1]: tp2_nc.service: Scheduled restart job, restart counter is at 2.
░░ Subject: Automatic restarting of a unit has been scheduled
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ Automatic restarting of the unit tp2_nc.service has been scheduled, as the result for
░░ the configured Restart= setting for the unit
```
