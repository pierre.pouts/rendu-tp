# I. First steps
🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

TEAMS  
IP serveur : 8.8.4.4  
PORT serveur : 443   
PORT client : 54261

```powershell
pierr@LAPTOP-MDL3A6CL MINGW64 ~
$  netstat -b -n

Connexions actives

  Proto  Adresse locale         Adresse distante       ▒tat
  TCP    10.33.16.238:49253     35.186.224.47:443      ESTABLISHED
 [Discord.exe]
  TCP    10.33.16.238:49516     20.90.153.243:443      ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    10.33.16.238:54237     52.112.238.197:443     TIME_WAIT
  TCP    10.33.16.238:54240     185.26.182.124:443     TIME_WAIT
  TCP    10.33.16.238:54241     185.26.182.124:443     TIME_WAIT
  TCP    10.33.16.238:54242     52.113.194.132:443     TIME_WAIT
  TCP    10.33.16.238:54243     52.112.238.101:443     TIME_WAIT
  TCP    10.33.16.238:54245     52.182.141.63:443      ESTABLISHED
 [SearchHost.exe]
  TCP    10.33.16.238:54246     96.16.249.55:443       ESTABLISHED
 [SearchHost.exe]
  TCP    10.33.16.238:54249     52.97.165.146:443      ESTABLISHED
 [SearchHost.exe]
  TCP    10.33.16.238:54250     13.107.6.158:443       ESTABLISHED
 [SearchHost.exe]
  TCP    10.33.16.238:54256     52.113.194.132:443     ESTABLISHED
 [Teams.exe]
  TCP    10.33.16.238:54257     52.113.194.132:443     ESTABLISHED
 [Teams.exe]
  TCP    10.33.16.238:54258     52.114.74.96:443       ESTABLISHED
 [Teams.exe]
  TCP    10.33.16.238:54259     52.112.238.32:443      ESTABLISHED
 [Teams.exe]
  TCP    10.33.16.238:54260     52.113.205.19:443      ESTABLISHED
 [Teams.exe]
  TCP    10.33.16.238:54261     8.8.4.4:443            ESTABLISHED

```
[Ma capture pcap TEAMS](./TCP-teams.pcapng)

---

DISCORD  
IP serveur : 20.42.73.26   
PORT serveur : 443   
PORT client : 49769

```powershell
pierr@LAPTOP-MDL3A6CL MINGW64 ~
$  netstat -b -n

Connexions actives

  Proto  Adresse locale         Adresse distante       ▒tat
  TCP    10.33.16.238:49516     20.90.153.243:443      ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    10.33.16.238:49768     162.159.135.232:443    ESTABLISHED
 [Discord.exe]
  TCP    10.33.16.238:49769     20.42.73.26:443        ESTABLISHED

```
[Ma capture pcap DISCORD](./TCP-teams.pcapng)

---

YOUTUBE  
IP serveur : 51.124.78.146   
PORT serveur : 443  
PORT client : 58504

```powershell

pierr@LAPTOP-MDL3A6CL MINGW64 ~
$  netstat -b -n

Connexions actives

  Proto  Adresse locale         Adresse distante       ▒tat
  TCP    10.33.16.238:49516     20.90.153.243:443      ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    10.33.16.238:58487     204.79.197.239:443     ESTABLISHED
 [msedge.exe]
  TCP    10.33.16.238:58497     185.26.182.94:443      ESTABLISHED
 [opera.exe]
  TCP    10.33.16.238:58501     185.26.182.118:443     ESTABLISHED
 [opera.exe]
  TCP    10.33.16.238:58502     216.58.204.99:443      ESTABLISHED
 [opera.exe]
  TCP    10.33.16.238:58503     185.26.182.94:443      ESTABLISHED
 [opera.exe]
  TCP    10.33.16.238:58504     51.124.78.146:443      TIME_WAIT

  ```
  [Ma capture pcap YOUTUBE](./TCP-youtube.pcapng)

---

 Video YOUTUBE  
IP serveur : 216.58.214.78  
PORT serveur : 443   
PORT client : 62538

[Ma capture pcap VideoYOUTUBE](./UDP-videoyoutube.pcapng) 

---
  
F1 MAnager 2022  
IP serveur : 162.159.138.232  
PORT serveur : 443   
PORT client : 53386

[Ma capture pcap F1Manager](./UDP-F1Manager.pcapng)

---

# II. Mise en place

## 1. SSH

---

🌞 **Examinez le trafic dans Wireshark**

- **déterminez si SSH utilise TCP ou UDP**  
  -SSH utilise le TCP 

- **repérez le *3-Way Handshake* à l'établissement de la connexion**  
[Ma capture pcap 3-Way](./3-Way.pcapng)

- **repérez du trafic SSH**  
[Ma capture pcap Trafique SSH](./Trafique-SSH.pcapng)    

🌞 **Demandez aux OS** 

Avec windows
```
PS C:\Windows\system32> netstat -b -n

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    10.4.1.1:50734         10.4.1.11:22           ESTABLISHED
 [ssh.exe]
 ```
 Avec ma vm  

 ```
 [pierre@node1 ~]$ ss
Netid    State    Recv-Q    Send-Q                      Local Address:Port          Peer Address:Port
tcp      ESTAB    0         52                              10.4.1.11:ssh               10.4.1.1:50734
```
---

# III. DNS

## 1. Présentation

---

🌞 **Dans le rendu, je veux**

- un `cat` des fichiers de conf
```powershell
[pierre@dns-serveur ~]$ sudo cat /etc/named.conf

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;
        dnssec-validation yes;
        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";
        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};
logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};
# référence vers notre fichier de zone
zone "tp4.b1" IN {
     type master;
     file "tp4.b1.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse
zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp4.b1.rev";
     allow-update { none; };
     allow-query { any; };
};
```


- un `systemctl status named` qui prouve que le service tourne bien
```powershell
[pierre@dns-server /]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-10-25 13:45:12 CEST; 33s ago
```



- une commande `ss` qui prouve que le service écoute bien sur un port
```powershell
[pierre@dns-server etc]$ sudo  ss -alntp
State      Recv-Q     Send-Q         Local Address:Port          Peer Address:Port     Process
LISTEN     0          10                10.4.1.201:53                 0.0.0.0:*         users:(("named",pid=1552,fd=21))
```

🌞 **Ouvrez le bon port dans le firewall**

- grâce à la commande `ss` vous devrez avoir repéré sur quel port tourne le service  
Le port 53  


```powershell
[pierre@dns-serveur etc]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[pierre@dns-serveur etc]$ sudo firewall-cmd --reload
success
```


## 3. Test

---

🌞 **Sur la machine `node1.tp4.b1`**
```powershell
[pierre@node1 ~]$ dig dns-server.tp4.b1

; <<>> DiG 9.16.23-RH <<>> dns-server.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57615
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 9b711f49ccabe622010000006357e7290c5df5bd63baa1ad (good)
;; QUESTION SECTION:
;dns-server.tp4.b1.             IN      A

;; ANSWER SECTION:
dns-server.tp4.b1.      86400   IN      A       10.4.1.201

;; Query time: 1 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Tue Oct 25 16:22:03 CEST 2022
;; MSG SIZE  rcvd: 90
```
```powershell
[pierre@node1 ~]$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 33888
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: bd516d4a8c0468ab010000006357e702a93ad2652cdcb9a2 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             136     IN      A       216.58.214.174

;; Query time: 0 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Tue Oct 25 16:21:27 CEST 2022
;; MSG SIZE  rcvd: 83
```

🌞 **Sur votre PC**

```powershell
PS C:\Windows\system32> nslookup node1.tp4.b1 10.4.1.201
Serveur :   dns-server.tp4.b1
Address:  10.4.1.201

Nom :    node1.tp4.b1
Address:  10.4.1.11
```

[Ma capture pcap requete DNS](./DNS.pcapng)  



