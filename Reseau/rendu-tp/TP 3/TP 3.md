
### 1. Echange ARP

🌞**Générer des requêtes ARP**

```
ping 10.3.1.12
```
```
ip neigh show
10.3.1.12 dev enp0s8 lladdr 08:00:27:33:86:72 STABLE
```
```
MAC `john`: 08:00:27:f1:57:37
MAC `marcel`: 08:00:27:33:86:72
```
```
ip neigh show 10.3.1.12
ip link show 
```


### 2. Analyse de trames

🌞**Analyse de trames**

```
sudo tcpdump -i enp0s8 -c 10 -w Tp3_arp.pcap arp
```
```
sudo ip neigh flush all
```
```
ping 10.3.1.11
```



🦈 **Capture réseau `tp3_arp.pcapng`** qui contient un ARP

- [tp3_arp](./tp3_arp.pcap)

## II. Routage

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**
```
sudo firewall-cmd --add-masquerade --zone=public  
success
```
```
sudo firewall-cmd --add-masquerade --zone=public --permanent  
success
```
```
sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```
🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

```
sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s8  
sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8
```
```
ping 10.3.2.12*
```

### 2. Analyse de trames

🌞**Analyse des échanges ARP**

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         |`marcel` `AA:BB:CC:DD:EE`| x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         |router 08:00:27:2f:71:d6 | x              | `marcel` `AA:BB:CC:DD:EE`  |
| ...   | ...         | ...       | ...                     |                |                            |
| ?     | Ping        |john 10.3.1.11 | john 08:00:27:a4:21:98 | router 10.3.1.254 | router 08:00:27:2f:71:d6  |
| ?     | Pong        |router 10.3.1.254 | router 08:00:27:2f:71:d6 | john 10.3.1.11 | john 08:00:27:a4:21:98   |



🦈 **Capture réseau `tp3_routage_marcel.pcapng`**    

- [tp3_routage-marcel](./tp3_routage-marcel.pcap)


### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

```
sudo nano /etc/sysconfig/network  
GATEWAY=10.0.2.15    
systemctl restart NetworkManager
```
```
ping 8.8.8.8
```
```
servername 8.8.8.8 devient 
google.com 8.8.8.8
```
```
dig google.com 
; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 43165
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             188     IN      A       216.58.198.206

;; Query time: 43 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 12:08:31 CEST 2022
;; MSG SIZE  rcvd: 55
```
```
ping google.com
```


🌞**Analyse de trames**



| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |     |
|-------|------------|--------------------|-------------------------|----------------|-----------------|-----|
| 1     | ping       | `john` `10.3.1.11` | `john` `08:00:27:a42198`| `8.8.8.8`      |`08:00:27:9a:dd:5a`|  |
| 2     | pong       | `8.8.8.8`          | `08:00:27:9a:dd:5a`     |`john` `10.3.1.11`|`john` `08:00:27:a42198` | |

🦈 Capture réseau tp3_routage_internet.pcapng  

- [tp3_ping-google.com](./tp3_ping-google.com)

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine  | `10.3.1.0/24`              | `10.3.2.0/24` |
|----------|----------------------------|---------------|
| `router` | `10.3.1.254`               | `10.3.2.254`  |
| `john`   | `10.3.1.11`                | no            |
| `bob`    | oui mais pas d'IP statique | no            |
| `marcel` | no                         | `10.3.2.12`   |

```schema
   john               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   dhcp        │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`
```

- créer une machine `bob`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
- récupérez de nouveau une IP en DHCP sur `bob` pour tester :
  - `bob` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP
    - vérifier qu'il peut `ping` sa passerelle
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
    - vérifier que la route fonctionne avec un `ping` vers une IP
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne
    - vérifier un `ping` vers un nom de domaine

### 2. Analyse de trames

🌞**Analyse de trames**

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
- demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcapng`

🦈 **Capture réseau `tp3_dhcp.pcapng`**